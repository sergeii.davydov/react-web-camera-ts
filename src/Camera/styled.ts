import styled from "styled-components/macro";

interface ScVideoCanvasProps {
  isHide?: boolean;
}

export const ScCamera = styled.div`
  max-width: 500px;
  width: 100%;
  margin: 30px auto;
  box-shadow: 0px 4px 8px 0px rgba(34, 60, 80, 0.2);
  background-color: #ffffff;
`;

export const ScCameraHeader = styled.div`
  padding: 20px 30px;
  font-size: 1.5em;
`;

export const ScVideo = styled.video<ScVideoCanvasProps>`
  width: 100%;
  vertical-align: top;
  display: ${({ isHide }) => (isHide ? "none" : "block")};
`;

export const ScCanvas = styled.canvas<ScVideoCanvasProps>`
  width: 100%;
  vertical-align: top;
  display: ${({ isHide }) => (isHide ? "none" : "block")};
`;

export const ScCameraBody = styled.div``;

export const ScCameraFooter = styled.div`
  padding: 20px 30px;
  text-align: center;
  button {
    margin: 0 10px;
  }
`;
