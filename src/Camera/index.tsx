import React, {
  ChangeEvent,
  FC,
  useCallback,
  useMemo,
  useRef,
  useState,
} from "react";

import { useUserMedia } from "./hooks";

import {
  ScCamera,
  ScCameraBody,
  ScCameraHeader,
  ScCameraFooter,
  ScVideo,
  ScCanvas,
} from "./styled";

const CAPTURE_OPTIONS = {
  audio: false,
  video: { facingMode: "environment" },
};

enum CameraStatus {
  ON,
  OFF,
}

interface CameraProps {
  onSave: (blob: File) => void;
}

const blobToFile = (theBlob: Blob, fileName: string): File => {
  return new File([theBlob], fileName, {
    lastModified: new Date().getTime(),
    type: theBlob.type,
  });
};

export const Camera: FC<CameraProps> = ({ onSave }) => {
  const videoRef = useRef<HTMLVideoElement>(null);
  const canvasRef = useRef<HTMLCanvasElement>(null);
  const [statusCamera, setStatusCamera] = useState<CameraStatus>(
    CameraStatus.ON
  );

  const mediaStream = useUserMedia(CAPTURE_OPTIONS);

  if (mediaStream && videoRef.current && !videoRef.current.srcObject) {
    videoRef.current.srcObject = mediaStream;
  }

  const handleCanPlay = () => {
    if (videoRef.current == null) return;

    videoRef.current.play();
  };

  const handleTakePhoto = () => {
    if (canvasRef.current == null || videoRef.current == null) return;

    const width = videoRef.current.offsetWidth;
    const height =
      videoRef.current.videoHeight / (videoRef.current.videoWidth / width);

    canvasRef.current.width = width;
    canvasRef.current.height = height;

    const context = canvasRef.current.getContext("2d");

    if (context == null) return;

    setStatusCamera(CameraStatus.OFF);
    context.drawImage(videoRef.current, 0, 0, width, height);
  };

  const handleReset = () => {
    if (canvasRef.current == null) return;

    const context = canvasRef.current.getContext("2d");

    if (context == null) return;

    context.clearRect(0, 0, 0, 0);
    setStatusCamera(CameraStatus.ON);
  };

  const handleApply = useCallback(() => {
    if (canvasRef.current == null) return;

    canvasRef.current.toBlob(
      (blob) => {
        if (blob == null) return;
        const file = blobToFile(blob, "webcam.jpg");
        onSave(file);
      },
      "image/jpeg",
      1
    );
  }, [onSave]);

  const handleChangeInputFile = (e: ChangeEvent<HTMLInputElement>) => {
    if (e.target.files == null || e.target.files.length === 0) return;

    const file = e.target.files[0];
    onSave(file);
  };

  const buttons = useMemo(() => {
    switch (statusCamera) {
      case CameraStatus.ON:
        return (
          <button type="button" onClick={handleTakePhoto}>
            Take photo
          </button>
        );
      case CameraStatus.OFF:
        return (
          <>
            <button type="button" onClick={handleReset}>
              Reset
            </button>
            <button type="button" onClick={handleApply}>
              Apply
            </button>
          </>
        );
      default:
        return null;
    }
  }, [statusCamera, handleApply]);

  return (
    <ScCamera>
      <ScCameraHeader>Camera</ScCameraHeader>
      <ScCameraBody>
        <ScVideo
          isHide={statusCamera === CameraStatus.OFF}
          ref={videoRef}
          onCanPlay={handleCanPlay}
          autoPlay
          playsInline
          muted
        />
        <ScCanvas isHide={statusCamera === CameraStatus.ON} ref={canvasRef} />
        <input
          accept="image/*"
          type="file"
          capture="user"
          onChange={handleChangeInputFile}
        />
      </ScCameraBody>
      <ScCameraFooter>{buttons}</ScCameraFooter>
    </ScCamera>
  );
};
