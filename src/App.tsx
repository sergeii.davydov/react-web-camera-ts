import React, { useState } from "react";

import { Camera } from "./Camera";

function App() {
  const [status, setStatus] = useState(false);

  const handleClick = () => {
    setStatus((s) => !s);
  };

  const handleSavePhoto = (file: File) => {
    handleClick();
    console.log(file);
  };

  return (
    <div className="App">
      <div className="btns-group">
        <button type="button" onClick={handleClick}>
          Turn {status ? "off" : "on"} camera
        </button>
      </div>
      {status && <Camera onSave={handleSavePhoto} />}
    </div>
  );
}

export default App;
